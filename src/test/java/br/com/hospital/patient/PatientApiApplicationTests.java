package br.com.hospital.patient;

import java.io.IOException;

import org.springframework.http.ResponseEntity;

import br.com.hospital.patient.generic.util.JsonUtil;
import br.com.hospital.patient.security.entity.dto.LoginDTO;
import br.com.hospital.patient.security.entity.dto.UserDTO;
import br.com.hospital.patient.util.PatientApiApplicationUtil;

public class PatientApiApplicationTests extends PatientApiApplicationUtil{
	
	public UserDTO autenticateUser() throws IOException {
		
		LoginDTO loginDTO = new LoginDTO();

		loginDTO.setUsername("raul");
		loginDTO.setPassword("123456");
		
		return autenticateUser(loginDTO);
	}
	
	public UserDTO autenticateUser(String userName, String passWord) throws IOException {
		
		LoginDTO loginDTO = new LoginDTO();

		loginDTO.setUsername(userName);
		loginDTO.setPassword(passWord);
		
		return autenticateUser(loginDTO);
	}
    
	public UserDTO autenticateUser(LoginDTO loginDTO) throws IOException {
		
		String path = "/user/v1/";
		Boolean userToken = Boolean.FALSE;

		ResponseEntity<String> response = createRestPost(loginDTO, path, userToken);
		
		UserDTO userDTO = JsonUtil.getInstance().mapFromJson(response.getBody(), UserDTO.class);

		setToken(userDTO.getToken());
		
		return userDTO;
	}

}
