package br.com.hospital.patient.util;

import java.util.List;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;

import br.com.hospital.patient.generic.util.JsonUtil;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PatientApiApplicationUtil {

	@LocalServerPort
	private int port;
	private TestRestTemplate restTemplate = new TestRestTemplate();
	

	HttpHeaders headers = new HttpHeaders();
	private String token;

	public String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

	public HttpHeaders getHeaders(Boolean userToken) {

		this.headers.add("Content-Type", "application/json");
		if (userToken) {
			String authHeader = "Bearer " + token;
			this.headers.add("Authorization", authHeader);
		}
		return this.headers;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public <T>  T createRest(Class<T> setType, Object object, String path, HttpMethod httpMethod, Boolean userToken)
			throws JsonProcessingException {

		HttpEntity<String> entity = getHttpEntity(object, userToken);
		
		ResponseEntity<T> response = restTemplate.exchange(createURLWithPort(path), httpMethod, entity, setType);

		return response.getBody();
	}

	public ResponseEntity<String> createRestPost(Object object, String path, Boolean userToken)
			throws JsonProcessingException {

		HttpEntity<String> entity = getHttpEntity(object, userToken);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path), HttpMethod.POST, entity,
				String.class);

		return response;
	}
	
	public ResponseEntity<String> createRestPut(Object object, String path, Boolean userToken)
			throws JsonProcessingException {

		HttpEntity<String> entity = getHttpEntity(object, userToken);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort(path), HttpMethod.PUT, entity,
				String.class);

		return response;
	}

	protected HttpEntity<String> getHttpEntity(Object object, Boolean userToken) throws JsonProcessingException {
		HttpEntity<String> entity;
		if(object == null) {
			entity = new HttpEntity<>(this.getHeaders(userToken));
		}else {
			entity = new HttpEntity<String>(JsonUtil.getInstance().mapToJson(object), this.getHeaders(userToken));			
		}
		return entity;
	}
	
	public <T> List<T> createRestResponseList(Class<T[]> setType, Object object, String path, HttpMethod method , Boolean userToken) {
	    HttpEntity<?> body = new HttpEntity<>(object, this.getHeaders(userToken));
	    ResponseEntity<T[]> response = restTemplate.exchange(createURLWithPort(path), method, body, setType);
	    return Lists.newArrayList(response.getBody());
	}
	
	public TestRestTemplate getRestTemplate() {
		return restTemplate;
	}
	
}
