package br.com.hospital.patient.generic.controller;



import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;

import br.com.hospital.patient.PatientApiApplicationTests;
import br.com.hospital.patient.generic.dto.EnderecoDTO;

public class EnderecoControllerTest  extends PatientApiApplicationTests {
	
	@Test
	void search() throws IOException {
		
		autenticateUser();
		
		String cep = "01451912";
		String path = "/endereco/v1/" + cep;
		Boolean userToken = Boolean.TRUE;
		
		EnderecoDTO enderecoDTO = createRest(EnderecoDTO.class, null, path, HttpMethod.GET, userToken);
		
		assertNotNull(enderecoDTO);
		
		
	}

}
