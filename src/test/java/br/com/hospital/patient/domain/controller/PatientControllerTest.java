package br.com.hospital.patient.domain.controller;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.hospital.patient.PatientApiApplicationTests;
import br.com.hospital.patient.domain.entity.dto.PatientDTO;
import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.security.entity.dto.UserDTO;

class PatientControllerTest extends PatientApiApplicationTests {
	
	@Test
	void testSearch() throws IOException {

		autenticateUser();

		String name = "manuela";
		String path = "/patient/v1/search/" + name;
		Boolean userToken = Boolean.TRUE;

		List<PatientDTO> list = createRestResponseList(PatientDTO[].class, name, path, HttpMethod.GET, userToken);

		list.forEach(patientDTO -> {

			assertNotNull(patientDTO);
			assertNotNull(patientDTO.getId());

			assertEquals(name, patientDTO.getName());
		});
	}

	@Test
	void testfindPagination() throws IOException {

		autenticateUser();

		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "0");
		map.put("size", "10");

		PatientDTO dto = new PatientDTO();
		dto.setName("viviane");

		String path = "/user/v1/pagination";
		Boolean userToken = Boolean.TRUE;

		ResponseEntity<ResponsePageDTO<PatientDTO>> response = getRestTemplate().exchange(createURLWithPort(path),
				HttpMethod.PUT, getHttpEntity(dto, userToken), new ParameterizedTypeReference<ResponsePageDTO<PatientDTO>>() {
				});

		ResponsePageDTO<PatientDTO> responsePage = response.getBody();
		
		List<PatientDTO> list = responsePage.getContent();

		list.forEach(patientDTO -> {

			assertNotNull(patientDTO);
			assertNotNull(patientDTO.getId());


			assertEquals(dto.getName(), patientDTO.getName());
		});

	}

	public void testRegister() {

	}
	
	@Test
	void testUpdate() throws IOException {
		
		UserDTO userDTO = autenticateUser();

		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "0");
		map.put("size", "10");

		PatientDTO dto = new PatientDTO();
		dto.setName("manuela");

		String path = "/user/v1/pagination";
		Boolean userToken = Boolean.TRUE;

		ResponseEntity<ResponsePageDTO<PatientDTO>> response = getRestTemplate().exchange(createURLWithPort(path),
				HttpMethod.PUT, getHttpEntity(dto, userToken), new ParameterizedTypeReference<ResponsePageDTO<PatientDTO>>() {
				});

		ResponsePageDTO<PatientDTO> responsePage = response.getBody();
		
		List<PatientDTO> list = responsePage.getContent();

		list.forEach(patientDTO -> {

			assertNotNull(patientDTO);
			assertNotNull(patientDTO.getId());

			assertEquals(dto.getName(), patientDTO.getName());
			
			String pathUpdateUser = "/user/v1/updateUser/"+patientDTO.getId();
			
			patientDTO.setName("Test");
			patientDTO.setUserChange(userDTO);
						
			try {
				ResponseEntity<PatientDTO> createRestPost = getRestTemplate().exchange(createURLWithPort(pathUpdateUser), HttpMethod.PUT,
						getHttpEntity(patientDTO, userToken), PatientDTO.class);
				
				PatientDTO patientDTOUpdate = createRestPost.getBody();
				
				assertNotNull(patientDTOUpdate);
				assertNotNull(patientDTOUpdate.getId());

				assertEquals(patientDTO.getName(), patientDTOUpdate.getName());
				
			} catch (JsonProcessingException e) {			
				assertTrue(false);
			}
			
		});

	}

	@Test
	void testChangeStatus() throws IOException {
		
		autenticateUser();

		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "0");
		map.put("size", "10");

		PatientDTO dto = new PatientDTO();
		dto.setName("mauricio");

		String path = "/user/v1/pagination";
		Boolean userToken = Boolean.TRUE;

		ResponseEntity<ResponsePageDTO<PatientDTO>> response = getRestTemplate().exchange(createURLWithPort(path),
				HttpMethod.PUT, getHttpEntity(dto, userToken), new ParameterizedTypeReference<ResponsePageDTO<PatientDTO>>() {
				});

		ResponsePageDTO<PatientDTO> responsePage = response.getBody();
		
		List<PatientDTO> list = responsePage.getContent();

		list.forEach(patientDTO -> {

			assertNotNull(patientDTO);
			assertNotNull(patientDTO.getId());

			assertEquals(dto.getName(), patientDTO.getName());
			
			String pathUpdateUser = "/user/v1/change_status/";
						
			try {
				ResponseEntity<PatientDTO> createRestPost = getRestTemplate().exchange(createURLWithPort(pathUpdateUser), HttpMethod.POST,
						getHttpEntity(patientDTO, userToken), PatientDTO.class);
				
				PatientDTO patientDTOUpdate = createRestPost.getBody();
				
				assertNotNull(patientDTOUpdate);
				assertNotNull(patientDTOUpdate.getId());

				assertEquals(patientDTO.getName(), patientDTOUpdate.getName());
				assertEquals("I", patientDTOUpdate.getStatus());
				
			} catch (JsonProcessingException e) {			
				assertTrue(false);
			}
			
		});

	}

}
