package br.com.hospital.patient.domain.security.controller;



import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.hospital.patient.PatientApiApplicationTests;
import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.security.entity.dto.LoginDTO;
import br.com.hospital.patient.security.entity.dto.UserDTO;

class UserControllerTest extends PatientApiApplicationTests {

	@Test
	void testAuthenticateUser() throws Exception {

		LoginDTO loginDTO = new LoginDTO();

		loginDTO.setUsername("heitor");
		loginDTO.setPassword("123456");

		UserDTO userDTO = autenticateUser(loginDTO);

		assertNotNull(userDTO);
		assertNotNull(userDTO.getId());

		assertNotNull(userDTO.getToken());
		assertEquals(loginDTO.getUsername(), userDTO.getUsername());
	}

	@Test
	void testSearch() throws IOException {

		autenticateUser();

		String name = "theo";
		String path = "/user/v1/search/" + name;
		Boolean userToken = Boolean.TRUE;

		List<UserDTO> list = createRestResponseList(UserDTO[].class, name, path, HttpMethod.GET, userToken);

		list.forEach(userDTO -> {

			assertNotNull(userDTO);
			assertNotNull(userDTO.getId());

			setToken(userDTO.getToken());

			assertEquals(name, userDTO.getUsername());
		});
	}

	@Test
	void testfindPagination() throws IOException {

		autenticateUser();

		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "0");
		map.put("size", "10");

		UserDTO dto = new UserDTO();
		dto.setUsername("silvio");

		String path = "/user/v1/pagination";
		Boolean userToken = Boolean.TRUE;

		ResponseEntity<ResponsePageDTO<UserDTO>> response = getRestTemplate().exchange(createURLWithPort(path),
				HttpMethod.PUT, getHttpEntity(dto, userToken), new ParameterizedTypeReference<ResponsePageDTO<UserDTO>>() {
				});

		ResponsePageDTO<UserDTO> responsePage = response.getBody();
		
		List<UserDTO> list = responsePage.getContent();

		list.forEach(userDTO -> {

			assertNotNull(userDTO);
			assertNotNull(userDTO.getId());

			setToken(userDTO.getToken());

			assertEquals(dto.getUsername(), userDTO.getUsername());
		});

	}

	public void testRegisterUser() {

	}
	
	@Test
	void testUpdateUser() throws IOException {
		
		UserDTO userDTOAutenticate = autenticateUser();

		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "0");
		map.put("size", "10");

		UserDTO dto = new UserDTO();
		dto.setUsername("silvio");

		String path = "/user/v1/pagination";
		Boolean userToken = Boolean.TRUE;

		ResponseEntity<ResponsePageDTO<UserDTO>> response = getRestTemplate().exchange(createURLWithPort(path),
				HttpMethod.PUT, getHttpEntity(dto, userToken), new ParameterizedTypeReference<ResponsePageDTO<UserDTO>>() {
				});

		ResponsePageDTO<UserDTO> responsePage = response.getBody();
		
		List<UserDTO> list = responsePage.getContent();

		list.forEach(userDTO -> {

			assertNotNull(userDTO);
			assertNotNull(userDTO.getId());

			setToken(userDTO.getToken());

			assertEquals(dto.getUsername(), userDTO.getUsername());
			
			String pathUpdateUser = "/user/v1/updateUser/"+userDTO.getId();
			
			userDTO.setName("Test");
			userDTO.setUserChangeId(userDTOAutenticate.getId());
						
			try {
				ResponseEntity<UserDTO> createRestPost = getRestTemplate().exchange(createURLWithPort(pathUpdateUser), HttpMethod.PUT,
						getHttpEntity(userDTO, userToken), UserDTO.class);
				
				UserDTO userDTOUpdate = createRestPost.getBody();
				
				assertNotNull(userDTOUpdate);
				assertNotNull(userDTOUpdate.getId());

				assertEquals(userDTO.getName(), userDTOUpdate.getName());
				
			} catch (JsonProcessingException e) {			
				assertTrue(false);
			}
			
		});

	}

	@Test
	void testChangeStatusUser() throws IOException {
		
		autenticateUser();

		Map<String, String> map = new HashMap<String, String>();
		map.put("page", "0");
		map.put("size", "10");

		UserDTO dto = new UserDTO();
		dto.setUsername("silvio");

		String path = "/user/v1/pagination";
		Boolean userToken = Boolean.TRUE;

		ResponseEntity<ResponsePageDTO<UserDTO>> response = getRestTemplate().exchange(createURLWithPort(path),
				HttpMethod.PUT, getHttpEntity(dto, userToken), new ParameterizedTypeReference<ResponsePageDTO<UserDTO>>() {
				});

		ResponsePageDTO<UserDTO> responsePage = response.getBody();
		
		List<UserDTO> list = responsePage.getContent();

		list.forEach(userDTO -> {

			assertNotNull(userDTO);
			assertNotNull(userDTO.getId());

			setToken(userDTO.getToken());

			assertEquals(dto.getUsername(), userDTO.getUsername());
			
			String pathUpdateUser = "/user/v1/change_status_user/";
						
			try {
				ResponseEntity<UserDTO> createRestPost = getRestTemplate().exchange(createURLWithPort(pathUpdateUser), HttpMethod.POST,
						getHttpEntity(userDTO, userToken), UserDTO.class);
				
				UserDTO userDTOUpdate = createRestPost.getBody();
				
				assertNotNull(userDTOUpdate);
				assertNotNull(userDTOUpdate.getId());

				assertEquals(userDTO.getName(), userDTOUpdate.getName());
				assertEquals("I", userDTOUpdate.getStatus());
				
			} catch (JsonProcessingException e) {			
				assertTrue(false);
			}
			
		});

	}

}
