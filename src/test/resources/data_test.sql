delete from user_roles;
delete from users;
delete from roles;
delete from patient;

INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('heitor@gmail.com', 'heitor Azevedo', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'heitor', null, 'A', CURRENT_TIMESTAMP() );
insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('theo@gmail.com', 'theo', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'theo', (select id from users where username = 'heitor') , 'A', CURRENT_TIMESTAMP() );
insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('silvio@gmail.com', 'silvio', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'silvio', (select id from users where username = 'heitor') , 'A', CURRENT_TIMESTAMP() );
insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('raul@gmail.com', 'raul', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'raul', (select id from users where username = 'heitor'), 'A', CURRENT_TIMESTAMP() );

insert into user_roles (user_id, role_id) values ((select id from users where username = 'heitor'), (select id from roles where name = 'ROLE_USER' ));
insert into user_roles (user_id, role_id) values ((select id from users where username = 'heitor'), (select id from roles where name = 'ROLE_ADMIN' ));

insert into user_roles (user_id, role_id) values ((select id from users where username = 'raul'), (select id from roles where name = 'ROLE_USER' ));
insert into user_roles (user_id, role_id) values ((select id from users where username = 'raul'), (select id from roles where name = 'ROLE_ADMIN' ));

insert into user_roles (user_id, role_id) values ((select id from users where username = 'theo'), (select id from roles where name = 'ROLE_ADMIN' ));

insert into user_roles (user_id, role_id) values ((select id from users where username = 'silvio'), (select id from roles where name = 'ROLE_USER' ));

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '67723688008', 'viviane@gmail.com', 'viviane', (select id from users where username = 'heitor'), 'A', CURRENT_TIMESTAMP(),
    'Avenida Vinte e Três de Maio', 2499, '', 'Bela Vista',  'São Paulo', 'São Paulo', '01316060', '11965478543', PARSEDATETIME(FORMATDATETIME('2000-02-11', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '10082984093', 'manuela@gmail.com', 'manuela', (select id from users where username = 'heitor'), 'A', CURRENT_TIMESTAMP(),
    'Praça da Sé', 28, '', 'Sé',  'São Paulo', 'São Paulo', '01001001', '11912659874', PARSEDATETIME(FORMATDATETIME('1982-02-28', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '92335704010', 'zelia@gmail.com', 'zelia', (select id from users where username = 'heitor'), 'A', CURRENT_TIMESTAMP(),
    'Rua Gilberto Sabino', 54, '', 'Pinheiros',  'São Paulo', 'São Paulo', '05425020', '11936141523', PARSEDATETIME(FORMATDATETIME('1990-01-26', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '40725725001', 'mauricio@gmail.com', 'mauricio', (select id from users where username = 'heitor'), 'A', CURRENT_TIMESTAMP(), 
    'Avenida Paulista', 900, '', 'Bela Vista', 'São Paulo', 'São Paulo', '01310940', '11985142536', PARSEDATETIME(FORMATDATETIME('1999-12-10', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '40725725001', 'eduardo@gmail.com', 'eduardo', (select id from users where username = 'heitor'), 'A', CURRENT_TIMESTAMP(),
    'Praça Carlos Gomes', 46, '10º Andar', 'Centro', 'São Paulo', 'São Paulo', '01501040', '1122282071', PARSEDATETIME(FORMATDATETIME('1979-01-06', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

