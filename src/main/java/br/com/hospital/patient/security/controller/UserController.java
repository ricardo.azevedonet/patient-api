package br.com.hospital.patient.security.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.exception.BusinessException;
import br.com.hospital.patient.generic.exception.UserNotFindException;
import br.com.hospital.patient.security.ManagerSecurityService;
import br.com.hospital.patient.security.entity.dto.LoginDTO;
import br.com.hospital.patient.security.entity.dto.UserDTO;


@RestController
@RequestMapping("/user/v1")
public class UserController {

    @Autowired
    private ManagerSecurityService managerSecurityService;
    
    @GetMapping("/search/{userName}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List<UserDTO>> search(@PathVariable String userName) {
        return ResponseEntity.ok( managerSecurityService.search(userName));
    } 
    
    @GetMapping("/")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List<UserDTO>> searchUsers(UserDTO userDTO) {
        return ResponseEntity.ok(managerSecurityService.findByUsernameContainingIgnoreCase(userDTO.getUsername()));
    }

    @PutMapping("/pagination")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")    
    public ResponseEntity<ResponsePageDTO<UserDTO>> findPagination(@RequestBody UserDTO userDTO, Pageable pageable) {
        return ResponseEntity.ok(managerSecurityService.getUserFromFiltrer(userDTO, pageable));
    }

    @PostMapping(value = "/")    
    public ResponseEntity<UserDTO> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {    	
        return ResponseEntity.ok(managerSecurityService.authenticateUser(loginDTO));
    }

    @PostMapping("/signup")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> registerUser(@Valid @RequestBody UserDTO userDTO) throws BusinessException {
        return ResponseEntity.ok(managerSecurityService.registerUser(userDTO));
    }

    @PutMapping("/updateUser/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> updateUser(@PathVariable(value = "id") Long id, @Valid @RequestBody UserDTO userDTO) throws BusinessException {
        return ResponseEntity.ok().body(managerSecurityService.updateUser(id, userDTO));
    }
    
    @PostMapping("/change_status_user")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> changeStatusUser(@Valid @RequestBody UserDTO userDTO) throws UserNotFindException {
        return ResponseEntity.ok(managerSecurityService.changeStatusUser(userDTO));
    }  
}