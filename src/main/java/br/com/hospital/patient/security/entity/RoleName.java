package br.com.hospital.patient.security.entity;

public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}
