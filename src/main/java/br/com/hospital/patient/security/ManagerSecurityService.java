package br.com.hospital.patient.security;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.enums.Status;
import br.com.hospital.patient.generic.exception.BusinessException;
import br.com.hospital.patient.generic.exception.UserNotFindException;
import br.com.hospital.patient.security.entity.Role;
import br.com.hospital.patient.security.entity.RoleName;
import br.com.hospital.patient.security.entity.User;
import br.com.hospital.patient.security.entity.dto.LoginDTO;
import br.com.hospital.patient.security.entity.dto.UserDTO;
import br.com.hospital.patient.security.repository.RoleRepository;
import br.com.hospital.patient.security.repository.UserRepository;

@Service
public class ManagerSecurityService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder encoder;

	public String createNewPassWord() {
		return UUID.randomUUID().toString();
	}

	public UserDTO registerUser(UserDTO userDTO) throws BusinessException {
		
		User.validate(userDTO);
		
		User user = new User(userDTO.getName(), userDTO.getUsername(), userDTO.getEmail(),
				encoder.encode(createNewPassWord()), Status.A, LocalDateTime.now(), 
				(userDTO.getUserChangeId() != null ? userDTO.getUserChangeId() : null ));

		List<String> strRoles = userDTO.getRoles();
		Set<Role> roles = new HashSet<>();
		
		strRoles.forEach(role -> {
			try {
				String authority = role;
				if ("admin".equals(authority)) {
					Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
								.orElseThrow(UserNotFindException::new);
					roles.add(adminRole);
				} else {
					Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
							.orElseThrow(UserNotFindException::new);
					roles.add(userRole);
				}
			} catch (UserNotFindException e) {
				throw new RuntimeException(e);
			}
		});
		
		if(roles.isEmpty()) {
			roles.add(roleRepository.findByName(RoleName.ROLE_USER)
					.orElseThrow(UserNotFindException::new));
		}
		
		user.setRoles(roles);
		
		return User.converteUserToUserDTO(userRepository.save(user));
	}

	public UserDTO updateUser(Long id, @Valid @RequestBody UserDTO userDTO) throws BusinessException {

		User.validate(userDTO);
		
		Optional<User> optional = userRepository.findById(id);
		if(!optional.isPresent()) {
			throw new UserNotFindException();

		}
		User user = optional.get();
		user.setName(userDTO.getName());
		user.setUsername(userDTO.getUsername());
		user.setEmail(userDTO.getEmail());
		user.setDateChange(LocalDateTime.now());
		user.setUserChangeId((userDTO.getUserChangeId() != null ? userDTO.getUserChangeId() : null ));

		User userUpdate = userRepository.save(user);

		return User.converteUserToUserDTO(userUpdate);
	}

	public UserDTO changeStatusUser(UserDTO userDTO) throws UserNotFindException {

		Optional<User> optional = userRepository.findById(userDTO.getId());
		if(!optional.isPresent()) {
			throw new UserNotFindException();

		}
		User user = optional.get();

		user.setStatus(user.getStatus().equals(Status.A) ? Status.I : Status.A);
		user.setUserChangeId(userDTO.getUserChangeId() != null ? userDTO.getUserChangeId() : null );
		user.setDateChange(LocalDateTime.now());

		return User.converteUserToUserDTO(userRepository.save(user));
	}

	public List<UserDTO> findByUsernameContainingIgnoreCase(String username) {
		return User.converteListUserToListUserDTO(userRepository.findByUsernameContainingIgnoreCase(username));
	}

	public ResponsePageDTO<UserDTO> getUserFromFiltrer(UserDTO userDTO, Pageable pageable) {
		Pageable sortedByIdDesc = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
				Sort.by("id").descending());
		Page<User> user = userRepository.findUser(userDTO, sortedByIdDesc);
		return User.convertPageUserToPageUserDTO(user);
	}

	public UserDTO authenticateUser(@Valid LoginDTO loginDTO) {
		return User.authenticateUser(loginDTO);
	}

	public List<UserDTO> search(String userName) {
		List<User> list = userRepository.findByUsernameContainingIgnoreCase(userName);
		List<UserDTO> ret = new ArrayList<>();
		list.forEach(user ->
			ret.add(new UserDTO(user.getId(), user.getUsername(), user.getEmail(), user.getName(),
					(user.getRoles().stream().map(role -> role.getName().name()).collect(Collectors.toList()))))
		);
		
		return User.converteListUserToListUserDTO(list);
	}

	public Long countUserActive() {
		return userRepository.countByStatus(Status.A);
	}

	public Long countUserInacActive() {
		return userRepository.countByStatus(Status.I);
	}
}
