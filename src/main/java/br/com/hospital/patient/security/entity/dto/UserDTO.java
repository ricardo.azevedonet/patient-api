package br.com.hospital.patient.security.entity.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UserDTO  implements Serializable{
   
	private static final long serialVersionUID = 5568529994825292561L;
	
	private Long id;
    private String token;
    private String type = "Bearer";
    private String name;
    private String username;
    private String email;
    private Long userChangeId;
    private String userChangeName;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateTimeStart;    
	
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateTimeEnd;
	
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateChange;
    
	private String status;
	private String password;
	private List<String> roles = Lists.newArrayList();
	
	public UserDTO() {}    

    public UserDTO(Long id, String accessToken, String username, String email, String name, List<String> roles) {
        this.id = id;
        this.token = accessToken;
        this.username = username;
        this.roles = roles;
        this.email = email;
        this.name = name;
    }

    public UserDTO(Long id, String username, String email, String name, List<String> roles) {
        this.id = id;
        this.username = username;
        this.roles = roles;
        this.email = email;
        this.name = name;
    }
    
    
}