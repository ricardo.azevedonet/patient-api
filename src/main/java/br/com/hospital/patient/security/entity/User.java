package br.com.hospital.patient.security.entity;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.google.common.collect.Lists;

import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.enums.Status;
import br.com.hospital.patient.generic.exception.BusinessException;
import br.com.hospital.patient.generic.exception.EmailIsAlreadyTakenException;
import br.com.hospital.patient.generic.exception.UsernameIsAlreadyException;
import br.com.hospital.patient.generic.util.ServiceLocator;
import br.com.hospital.patient.security.entity.dto.LoginDTO;
import br.com.hospital.patient.security.entity.dto.UserDTO;
import br.com.hospital.patient.security.jwt.JwtProvider;
import br.com.hospital.patient.security.repository.UserRepository;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
@Getter
@Setter
public class User{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(min=3, max = 50)
    private String name;

    @NotBlank
    @Size(min=3, max = 50)
    @Column(unique = true)
    private String username;

    @NaturalId
    @NotBlank
    @Size(max = 50)
    @Email
    @Column(unique = true)
    private String email;

    @NotBlank
    @Size(min=6, max = 100)
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @Column(name = "user_change_id")
    private Long userChangeId;

    @Transient
    private String userChangeName;

    @Nonnull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Nonnull
    @Column(name = "date_change")
    private LocalDateTime dateChange;

    public User() {}

    public User(String name, String username, String email, String password, Status status, LocalDateTime dateChange, Long userChangeId) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.status = status;
        this.dateChange = dateChange;
        this.userChangeId = userChangeId;
    }
    
    
    public static UserDTO converteUserToUserDTO(User user) {
		
		ModelMapper modelMapper = (ModelMapper) ServiceLocator.getInstance().getBean(ModelMapper.class);
		
		UserDTO userDTO = modelMapper.map(user, UserDTO.class);
		
		if(user.getUserChangeId()!= null) {
			UserRepository userRepository = (UserRepository) ServiceLocator.getInstance().getBean(UserRepository.class);
			Optional<User> optionalUser = userRepository.findById(user.getUserChangeId());
			if(optionalUser.isPresent()) {
				userDTO.setUserChangeId(optionalUser.get().getId());
				userDTO.setUserChangeName(optionalUser.get().getUsername());
			}
		}
		userDTO.setPassword(null);
		
		userDTO.setRoles(user.getRoles().stream().map(v -> v.getName().name()).collect(Collectors.toList()));
		
		return userDTO;
	}
	
	public static User converteUserDTOToUser(UserDTO userDTO) {
		ModelMapper modelMapper = (ModelMapper) ServiceLocator.getInstance().getBean(ModelMapper.class);
		
		User user = modelMapper.map(userDTO, User.class);
		
		if(userDTO.getUserChangeId()!= null) {			
			user.setUserChangeId(userDTO.getUserChangeId());
		}
				
		return user;
	}

	public static List<UserDTO> converteListUserToListUserDTO(List<User> listUser) {
		List<UserDTO> list = Lists.newArrayList();
		listUser.forEach( u ->
			list.add(User.converteUserToUserDTO(u))
		);		
		return list;
	}
	
	public static ResponsePageDTO<UserDTO> convertPageUserToPageUserDTO(Page<User> user) {
		Integer start = (int) user.getPageable().getOffset();
		Integer end = (start + user.getPageable().getPageSize()) > user.getContent().size() ? user.getContent().size() : (start + user.getPageable().getPageSize());
		
		List<UserDTO> list = User.converteListUserToListUserDTO(user.getContent().subList(start, end));
	
		ResponsePageDTO<UserDTO> page = new ResponsePageDTO<>();
		page.setContent(list);
		page.setTotalElements(user.getContent().size());
		
		return page;
	}
	
	public static UserDTO authenticateUser(@Valid LoginDTO loginRequest) {
		
		AuthenticationManager authenticationManager = (AuthenticationManager) ServiceLocator.getInstance().getBean(AuthenticationManager.class);
		JwtProvider jwtProvider = (JwtProvider) ServiceLocator.getInstance().getBean(JwtProvider.class);
		UserRepository userRepository = (UserRepository) ServiceLocator.getInstance().getBean(UserRepository.class);
		
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        User user = userRepository.findUserByUsername(userDetails.getUsername());
        
		return new UserDTO(user.getId(), jwt, userDetails.getUsername(), user.getEmail(), user.getName(),
				(userDetails.getAuthorities().stream().map(role -> role.getAuthority()).collect(Collectors.toList())));
	}
	
	public static void validate(UserDTO userDTO) throws BusinessException {
		
		UserRepository userRepository = (UserRepository) ServiceLocator.getInstance().getBean(UserRepository.class);
		
		Boolean result = userRepository.existsByUsernameAndIdNot(userDTO.getUsername(), userDTO.getId());
		
		if (result != null && result) {
			throw new UsernameIsAlreadyException();
		}
		
		result = userRepository.existsByEmailAndIdNot(userDTO.getEmail(), userDTO.getId());

		if (result != null && result) {		
			throw new EmailIsAlreadyTakenException();
		}
	}

}