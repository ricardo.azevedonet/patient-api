package br.com.hospital.patient.security.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.hospital.patient.generic.enums.Status;
import br.com.hospital.patient.security.entity.User;
import br.com.hospital.patient.security.entity.dto.UserDTO;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    User findUserByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    
    @Query("select u from User u where u.username = :username and u.id not in :id")
    Boolean existsByUsernameAndIdNot(@Param("username") String username, @Param("id") Long id);
    
    @Query("select u from User u where u.email = :email and u.id not in :id")
    Boolean existsByEmailAndIdNot(@Param("email") String email, @Param("id")  Long id);

    
	List<User> findByUsernameContainingIgnoreCase(String username);
	
	
	default Page<User> findUser(UserDTO userDTO, Pageable pageable){
		
		Specification<User> specification = (root, query, criteriaBuilder) -> {
		    List<Predicate> predicates = new ArrayList<>();
		    
		    if(userDTO.getUsername() != null) {
		        predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("username"), "%"+userDTO.getUsername()+"%")));
		    }
		    
		    if(userDTO.getName() != null) {
		        predicates.add(criteriaBuilder.and(criteriaBuilder.like(root.get("name"), "%"+userDTO.getName()+"%")));
		    }
		    
		    if(userDTO.getStatus() != null) {
		        predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("status"), 
		        		userDTO.getStatus().equalsIgnoreCase("A") ? Status.A : Status.I)));
		    }
		    			 
		    return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		}; 
		
		Page<User> p =  findAll(specification, pageable);
		
		p.forEach( u ->  u.setUserChangeName(u.getUserChangeId() != null ? findById(u.getUserChangeId()).get().getName() : null) );
		
		return findAll(specification, pageable);		
	}

	Page<User> findAll(Specification<User> specification, Pageable pageable);
	
	Long countByStatus(Status status);
	

}