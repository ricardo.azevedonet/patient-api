package br.com.hospital.patient.domain.entity.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardDTO implements Serializable {

	private static final long serialVersionUID = -5156645468498413774L;

	private Long sumActive;
	private Long sumInaActive;	
	private Long countUsers;
	private Long countPatients;	
	private Long usersCountActive;
	private Long usersCountInaActive;
	private Long patientsCountActive;
	private Long patientsCountInaActive;
	
}