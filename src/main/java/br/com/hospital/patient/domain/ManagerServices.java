package br.com.hospital.patient.domain;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.hospital.patient.domain.entity.dto.DashboardDTO;
import br.com.hospital.patient.domain.entity.dto.PatientDTO;
import br.com.hospital.patient.domain.exception.PatientNotFindException;
import br.com.hospital.patient.domain.services.PatientService;
import br.com.hospital.patient.generic.dto.GenericFilterDTO;
import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.exception.BusinessException;
import br.com.hospital.patient.security.ManagerSecurityService;

@Component
public class ManagerServices {
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private ManagerSecurityService managerSecurityService;
	
	public PatientDTO registerPatient(PatientDTO patientDTO) throws BusinessException {		
		return patientService.registerPatient(patientDTO);
	}

	public PatientDTO updatePatient(Long id, @Valid @RequestBody PatientDTO patientDTO) throws BusinessException {
		return patientService.updatePatient(id, patientDTO);
	}

	public PatientDTO changeStatusPatient(PatientDTO patientDTO) throws PatientNotFindException {
		return patientService.changeStatusPatient(patientDTO);
	}

	public List<PatientDTO> findByPatientnameContainingIgnoreCase(String name) {
		return patientService.findByPatientnameContainingIgnoreCase(name);
	}

	public ResponsePageDTO<PatientDTO> getPatientFromFiltrer(GenericFilterDTO genericFilterDTO, Pageable pageable) {		
		return patientService.getPatientFromFiltrer(genericFilterDTO, pageable);
	}

	public List<PatientDTO> search(String text) {		
		return patientService.search(text);
	}

	public DashboardDTO searchDashboard() {

		DashboardDTO dashboardDTO = new DashboardDTO();
		
		dashboardDTO.setPatientsCountActive(patientService.countActive());
		dashboardDTO.setPatientsCountInaActive(patientService.countInacActive());
		dashboardDTO.setCountPatients(dashboardDTO.getPatientsCountActive() + dashboardDTO.getPatientsCountInaActive());
		
		dashboardDTO.setUsersCountActive(managerSecurityService.countUserActive());
		dashboardDTO.setUsersCountInaActive(managerSecurityService.countUserInacActive());
		dashboardDTO.setCountUsers(dashboardDTO.getUsersCountActive() + dashboardDTO.getUsersCountInaActive());
		
		dashboardDTO.setSumActive(dashboardDTO.getPatientsCountActive() + dashboardDTO.getUsersCountActive());
		dashboardDTO.setSumInaActive(dashboardDTO.getPatientsCountInaActive() + dashboardDTO.getUsersCountInaActive() );
		return dashboardDTO;
	}

}
