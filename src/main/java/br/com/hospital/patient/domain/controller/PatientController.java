package br.com.hospital.patient.domain.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hospital.patient.domain.ManagerServices;
import br.com.hospital.patient.domain.entity.dto.PatientDTO;
import br.com.hospital.patient.domain.exception.PatientNotFindException;
import br.com.hospital.patient.generic.dto.GenericFilterDTO;
import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.exception.BusinessException;


@RestController
@RequestMapping("/patient/v1")
public class PatientController {

    @Autowired
    private ManagerServices managerServices;
    
    @GetMapping("/search/{name}")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List<PatientDTO>> search(@PathVariable String name) {
        return ResponseEntity.ok( managerServices.search(name));
    } 
    
    @GetMapping("/")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<List<PatientDTO>> searchPatients(PatientDTO patientDTO) {
        return ResponseEntity.ok(managerServices.findByPatientnameContainingIgnoreCase(patientDTO.getName()));
    }

    @PutMapping("/pagination")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")    
    public ResponseEntity<ResponsePageDTO<PatientDTO>> findPagination(@RequestBody GenericFilterDTO genericFilterDTO, Pageable pageable) {
    	
        return ResponseEntity.ok(managerServices.getPatientFromFiltrer(genericFilterDTO, pageable));
    }
  
    @PostMapping("/register")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PatientDTO> register(@Valid @RequestBody PatientDTO patientDTO) throws BusinessException {
        return ResponseEntity.ok(managerServices.registerPatient(patientDTO));
    }

    @PutMapping("/updatePatient/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PatientDTO> update(@PathVariable(value = "id") Long id, @Valid @RequestBody PatientDTO patientDTO) throws BusinessException {
        return ResponseEntity.ok().body(managerServices.updatePatient(id, patientDTO));
    }
    
    @PostMapping("/change_status")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<PatientDTO> changeStatus(@Valid @RequestBody PatientDTO patientDTO) throws PatientNotFindException {
        return ResponseEntity.ok(managerServices.changeStatusPatient(patientDTO));
    }  
}