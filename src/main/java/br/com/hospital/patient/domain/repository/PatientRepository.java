package br.com.hospital.patient.domain.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.hospital.patient.domain.entity.Patient;
import br.com.hospital.patient.generic.dto.GenericFilterDTO;
import br.com.hospital.patient.generic.enums.Status;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

	Optional<Patient> findByName(String name);

	Optional<Patient> findByCpf(String cpf);

	Patient findPatientByName(String name);

	Boolean existsByName(String name);

	Boolean existsByEmail(String email);

	@Query("select u from Patient u where u.name = :name and u.id not in :id")
	Boolean existsByPatientnameAndIdNot(@Param("name") String name, @Param("id") Long id);

	@Query("select u from Patient u where u.email = :email and u.id not in :id")
	Boolean existsByEmailAndIdNot(@Param("email") String email, @Param("id") Long id);

	default Page<Patient> findPatient(GenericFilterDTO genericFilterDTO, Pageable pageable) {

		Specification<Patient> specification = (root, query, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();

			if (genericFilterDTO.getName() != null) {
				predicates.add(
						criteriaBuilder.and(criteriaBuilder.like(root.get("name"), "%" + genericFilterDTO.getName() + "%")));
			}
			
			if (genericFilterDTO.getStatus() != null) {
				predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
						genericFilterDTO.getStatus().equals("A") ? Status.A : Status.I)));
			}

			return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		};

		return findAll(specification, pageable);
	}

	Page<Patient> findAll(Specification<Patient> specification, Pageable pageable);

	List<Patient> findByNameContainingIgnoreCase(String name);
	
	List<Patient> findByNameContainingIgnoreCaseOrNameContainingOrNameContainingIgnoreCase(String name, String cpf, String email);
	
	default Long countPatient(GenericFilterDTO genericFilterDTO, Pageable pageable) {

		Specification<Patient> specification = (root, query, criteriaBuilder) -> {
			List<Predicate> predicates = new ArrayList<>();

			if (genericFilterDTO.getName() != null) {
				predicates.add(
						criteriaBuilder.and(criteriaBuilder.like(root.get("name"), "%" + genericFilterDTO.getName() + "%")));
			}
			
			if (genericFilterDTO.getStatus() != null) {
				predicates.add(criteriaBuilder.and(criteriaBuilder.equal(root.get("status"),
						genericFilterDTO.getStatus().equals("A") ? Status.A : Status.I)));
			}

			return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
		};
		
		Page<Patient> page = findAll(specification, pageable);

		return page.getTotalElements();
	}
	
	Long countByStatus(Status status);

}