package br.com.hospital.patient.domain.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.hospital.patient.domain.entity.Patient;
import br.com.hospital.patient.domain.entity.dto.PatientDTO;
import br.com.hospital.patient.domain.exception.PatientNotFindException;
import br.com.hospital.patient.domain.repository.PatientRepository;
import br.com.hospital.patient.generic.dto.GenericFilterDTO;
import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.enums.Status;
import br.com.hospital.patient.generic.exception.BusinessException;
import br.com.hospital.patient.generic.util.ServiceLocator;
import br.com.hospital.patient.security.entity.User;
import br.com.hospital.patient.security.repository.UserRepository;

@Service
public class PatientService {

	@Autowired
	private PatientRepository patientRepository;
	
	public PatientDTO registerPatient(PatientDTO patientDTO) throws BusinessException {
		
		Patient.validate(patientDTO);
		
		Patient patient = new Patient(null, patientDTO.getName(), patientDTO.getCpf(), patientDTO.getEmail(), patientDTO.getAddress(), 
				patientDTO.getNumber(), patientDTO.getComplement(), 
				patientDTO.getDistrict(), patientDTO.getCity(),  patientDTO.getState(), patientDTO.getZipCode(), patientDTO.getTelefone(), patientDTO.getDataBirth(), 
				null, 
				Status.A, LocalDateTime.now());
		
		getUserChange(patientDTO, patient);
		
		return Patient.convertePatientToPatientDTO(patientRepository.save(patient));
	}

	public PatientDTO updatePatient(Long id, @Valid @RequestBody PatientDTO patientDTO) throws BusinessException {

		Patient.validate(patientDTO);
		
		Optional<Patient> optional = patientRepository.findById(id);
		if(!optional.isPresent()) {
			throw new PatientNotFindException();

		}
		Patient patient = optional.get();
		patient.setName(patientDTO.getName());
		patient.setCpf(patientDTO.getCpf());
		patient.setEmail(patientDTO.getEmail());
		patient.setDateChange(LocalDateTime.now());
		getUserChange(patientDTO, patient);

		Patient patientUpdate = patientRepository.save(patient);

		return Patient.convertePatientToPatientDTO(patientUpdate);
	}

	public PatientDTO changeStatusPatient(PatientDTO patientDTO) throws PatientNotFindException {

		Optional<Patient> optional = patientRepository.findById(patientDTO.getId());
		if(!optional.isPresent()) {
			throw new PatientNotFindException();

		}
		Patient patient = optional.get();

		patient.setStatus(patient.getStatus().equals(Status.A) ? Status.I : Status.A);
		getUserChange(patientDTO, patient);
		
		patient.setDateChange(LocalDateTime.now());

		return Patient.convertePatientToPatientDTO(patientRepository.save(patient));
	}

	private void getUserChange(PatientDTO patientDTO, Patient patient) {
		if(patientDTO.getUserChange() != null){
			UserRepository userRepository = (UserRepository) ServiceLocator.getInstance().getBean(UserRepository.class);
			 Optional<User> user = userRepository.findById(patientDTO.getUserChange().getId());
			 if(user.isPresent()){
				 patient.setUserChange(user.get());
			 }
		}
	}

	public List<PatientDTO> findByPatientnameContainingIgnoreCase(String name) {
		return Patient.converteListPatientToListPatientDTO(patientRepository.findByNameContainingIgnoreCase(name));
	}

	public ResponsePageDTO<PatientDTO> getPatientFromFiltrer(GenericFilterDTO genericFilterDTO, Pageable pageable) {
		Pageable sortedByIdDesc = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
				Sort.by("id").descending());
		Page<Patient> patient = patientRepository.findPatient(genericFilterDTO, sortedByIdDesc);
		
		
		sortedByIdDesc = PageRequest.of(pageable.getPageNumber(), 1000,
				Sort.by("id").descending());
		
		Long size = patientRepository.countPatient(genericFilterDTO, sortedByIdDesc);
		
		return Patient.convertPagePatientToPagePatientDTO(patient, size);
	}

	public List<PatientDTO> search(String text) {
		
		List<Patient> list = patientRepository.findByNameContainingIgnoreCaseOrNameContainingOrNameContainingIgnoreCase(text, text, text);
		
		return Patient.converteListPatientToListPatientDTO(list);
	}
		
	public Long countActive() {
		return patientRepository.countByStatus(Status.A);
	}

	public Long countInacActive() {
		return patientRepository.countByStatus(Status.I);
	}
}
