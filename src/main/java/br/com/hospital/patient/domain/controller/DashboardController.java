package br.com.hospital.patient.domain.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hospital.patient.domain.ManagerServices;
import br.com.hospital.patient.domain.entity.dto.DashboardDTO;


@RestController
@RequestMapping("/dashboard/v1")
public class DashboardController {

    @Autowired
    private ManagerServices managerServices;
    
    @GetMapping("/search/")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<DashboardDTO > search() {
        return ResponseEntity.ok().body( managerServices.searchDashboard());
    }    
}