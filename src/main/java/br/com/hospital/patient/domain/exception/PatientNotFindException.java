package br.com.hospital.patient.domain.exception;

import br.com.hospital.patient.generic.exception.BusinessException;

public class PatientNotFindException extends BusinessException  {

	private static final long serialVersionUID = 6530590518706095529L;

	@Override
	public String getCdError() {
		return "PATIENT.NOT.FIND";
	}

}
