package br.com.hospital.patient.domain.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;

import com.google.common.collect.Lists;

import br.com.hospital.patient.domain.entity.dto.PatientDTO;
import br.com.hospital.patient.domain.repository.PatientRepository;
import br.com.hospital.patient.generic.dto.ResponsePageDTO;
import br.com.hospital.patient.generic.enums.Status;
import br.com.hospital.patient.generic.exception.BusinessException;
import br.com.hospital.patient.generic.exception.EmailIsAlreadyTakenException;
import br.com.hospital.patient.generic.util.ServiceLocator;
import br.com.hospital.patient.security.entity.User;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "patient", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "email" }) })
@Getter
@Setter
public class Patient {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	@Size(min = 3, max = 250)
	private String name;

	@CPF
    private String cpf;

	@NotBlank
	@Email
	@Column(unique = true)
	private String email;
	
	@Nonnull
	private String address;
	
	@Nonnull
	private Long number;
	
	private String complement;
	
	@Nonnull
	private String district;
	
	@Nonnull
	private String city;
	
	@Nonnull
	private String state;
	
	@Nonnull
	@Column(name = "zip_code")
	private String zipCode;
	
	@Nonnull
	private String telefone;
		
	@Nonnull
	@Column(name = "date_birth")
	private LocalDateTime dataBirth;	

    @ManyToOne
    @JoinColumn(name = "user_change_id", foreignKey = @ForeignKey(name = "FK_USER_PATIENT"))
    private User userChange;

	@Nonnull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	@Nonnull
	@Column(name = "date_change")
	private LocalDateTime dateChange;

	public Patient() {
	}

	
	public static PatientDTO convertePatientToPatientDTO(Patient user) {

		ModelMapper modelMapper = (ModelMapper) ServiceLocator.getInstance().getBean(ModelMapper.class);

		return modelMapper.map(user, PatientDTO.class);
	}

	public static Patient convertePatientDTOToPatient(PatientDTO patientDTO) {
		ModelMapper modelMapper = (ModelMapper) ServiceLocator.getInstance().getBean(ModelMapper.class);

		Patient user = modelMapper.map(patientDTO, Patient.class);

		if (patientDTO.getUserChange() != null) {
			user.setUserChange(User.converteUserDTOToUser(patientDTO.getUserChange()));
		}

		return user;
	}

	public static List<PatientDTO> converteListPatientToListPatientDTO(List<Patient> listPatient) {
		List<PatientDTO> list = Lists.newArrayList();
		listPatient.forEach(u ->
			list.add(Patient.convertePatientToPatientDTO(u))
		);
		return list;
	}

	public static ResponsePageDTO<PatientDTO> convertPagePatientToPagePatientDTO(Page<Patient> user, Long size) {		
		List<PatientDTO> list = Patient.converteListPatientToListPatientDTO(user.getContent());

		ResponsePageDTO<PatientDTO> page = new ResponsePageDTO<>();
		page.setContent(list);
		page.setTotalElements(size);

		return page;
	}

	public static void validate(PatientDTO patientDTO) throws BusinessException {

		PatientRepository patientRepository = (PatientRepository) ServiceLocator.getInstance().getBean(PatientRepository.class);

		Boolean result = patientRepository.existsByEmailAndIdNot(patientDTO.getEmail(), patientDTO.getId());

		if (result != null && result) {
			throw new EmailIsAlreadyTakenException();
		}
	}


	public Patient(Long id, @NotBlank @Size(min = 3, max = 250) String name, @CPF String cpf,
			@NotBlank @Email String email, String address, Long number, String complement, String district, String city,
			String state, String zipCode, String telefone, LocalDateTime dataBirth, User userChange, Status status,
			LocalDateTime dateChange) {
		super();
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.email = email;
		this.address = address;
		this.number = number;
		this.complement = complement;
		this.district = district;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.telefone = telefone;
		this.dataBirth = dataBirth;
		this.userChange = userChange;
		this.status = status;
		this.dateChange = dateChange;
	}

}