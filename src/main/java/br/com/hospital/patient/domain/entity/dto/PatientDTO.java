package br.com.hospital.patient.domain.entity.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.annotation.Nonnull;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import br.com.hospital.patient.generic.enums.Status;
import br.com.hospital.patient.security.entity.dto.UserDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientDTO implements Serializable {

	private static final long serialVersionUID = -5156645468498413774L;

	private Long id;

	@NotBlank
	@Size(min = 3, max = 150)
	private String name;

	@Nonnull
	@CPF
    private String cpf;

	@NotBlank
	@Email
	private String email;

	private UserDTO userChange;

	@Nonnull
	@Enumerated(EnumType.STRING)
	private Status status;

	@Nonnull
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dateChange;
	
	@Nonnull
	private String address;
	
	@Nonnull
	private Long number;
	
	private String complement;
	
	@Nonnull
	private String district;
	
	@Nonnull
	private String city;
	
	@Nonnull
	private String state;
	
	@Nonnull	
	private String zipCode;
	
	@Nonnull
	private String telefone;
		
	@Nonnull	
	private LocalDateTime dataBirth;
	
}