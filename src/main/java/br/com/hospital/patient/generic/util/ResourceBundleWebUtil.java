/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.hospital.patient.generic.util;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * Classe utilitaria para retornar mensagens do arquivo msgBundle
 * 
 * @author
 */
@Component
public class ResourceBundleWebUtil {
	private final Map<Locale, ResourceBundle> mapBundles = new HashMap<>();

	/**
	 *
	 * @param key
	 * @return Mensagem com o locale padrao
	 */
	public String getString(final String key) {
		return getString(key, LocaleContextHolder.getLocale());
	}

	public String getString(String key, Object... params) {
		return MessageFormat.format(obterResourceBundle(LocaleContextHolder.getLocale()).getString(key), params);
	}

	/**
	 *
	 * @param key
	 * @param locale
	 * @return Mensagem com o locale passado no parametro
	 */
	public String getString(final String key, final Locale locale) {
		ResourceBundle rb = obterResourceBundle(locale);
		return rb.getString(key);
	}

	public ResourceBundle obterResourceBundle(final Locale locale) {
		ResourceBundle rb = mapBundles.get(locale);
		if (rb == null) {
			rb = ResourceBundle.getBundle("message", locale);
			mapBundles.put(locale, rb);
		}
		return rb;
	}

	public String obterMensagemInternacionalizada(String key) {
		return getString(key, LocaleContextHolder.getLocale());
	}

}
