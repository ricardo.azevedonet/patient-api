package br.com.hospital.patient.generic.exception;

public class UserNotFindException extends BusinessException {

	private static final long serialVersionUID = 2221335492586468221L;

	@Override
	public String getCdError() {		
		return "USERROLE.NOT.FIND";
	}

}
