package br.com.hospital.patient.generic.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtil {

	private static JsonUtil instance;

	public static JsonUtil getInstance() {
		return instance == null ? instance = new JsonUtil() : instance;
	}

	public String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	public <T> T mapFromJson(String json, Class<T> clazz) throws JsonProcessingException  {
		ObjectMapper objectMapper = new ObjectMapper();		
		return objectMapper.readValue(json, clazz);
	}

	
}
