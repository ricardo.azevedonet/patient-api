package br.com.hospital.patient.generic.dto;

import java.util.List;

public class ResponsePageDTO<T> {

	private long totalElements;
	private List<T> content;

	public ResponsePageDTO() {
	}

	public long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

}
