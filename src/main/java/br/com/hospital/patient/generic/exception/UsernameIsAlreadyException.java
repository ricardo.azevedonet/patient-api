package br.com.hospital.patient.generic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UsernameIsAlreadyException extends BusinessException {

	private static final long serialVersionUID = 1L;

	@Override
	public String getCdError() {
		return "USERNAME.IS.ALREADY";
	}
	
}
