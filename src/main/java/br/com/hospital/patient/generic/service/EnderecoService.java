package br.com.hospital.patient.generic.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.hospital.patient.generic.dto.EnderecoDTO;

@Service
public class EnderecoService {
	
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private HttpHeaders headers = new HttpHeaders();
	
	public EnderecoDTO obtemEnderecoPorCep(String cep) {
		
		this.headers.add("Content-Type", "application/json");
		
		HttpEntity<String> entity = new HttpEntity<>(this.headers);
		
		ResponseEntity<EnderecoDTO> response = restTemplate.exchange(String.format("https://viacep.com.br/ws/%s/json", cep), HttpMethod.GET, entity,
				EnderecoDTO.class);
		
		return response.getBody();
	}
	

}
