package br.com.hospital.patient.generic.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
public class JsonDTO implements Serializable {

	private static final long serialVersionUID = -391476803086907498L;

	private String locale;

	private Map<String, String> keys = new HashMap<>();

	public void addMensagen(MessagesDTO messagesTO) {
		keys.put(messagesTO.getChave(), messagesTO.getValor());
	}

}
