package br.com.hospital.patient.generic.exception;

public class UserRoleNotFindException extends BusinessException {

	private static final long serialVersionUID = 2221335492586468221L;

	@Override
	public String getCdError() {		
		return "USER.NOT.FIND";
	}

}
