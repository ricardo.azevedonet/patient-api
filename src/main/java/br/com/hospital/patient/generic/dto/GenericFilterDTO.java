package br.com.hospital.patient.generic.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericFilterDTO implements Serializable {
	
	private static final long serialVersionUID = 6447596274111520392L;

	private Long id;

    private String type;

    private String status;

    private String name;





}
