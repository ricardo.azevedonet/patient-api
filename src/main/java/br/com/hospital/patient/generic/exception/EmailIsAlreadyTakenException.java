package br.com.hospital.patient.generic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EmailIsAlreadyTakenException extends BusinessException {

	private static final long serialVersionUID = 1L;

	@Override
	public String getCdError() {
		return "EMAIL.IS.ALREADY.TAKEN";
	}
	
}
