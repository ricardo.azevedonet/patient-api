package br.com.hospital.patient.generic.enums;

import lombok.Getter;

@Getter
public enum Status {

	A("Active"), I("Inactive");

	private String description;

	/**
	 * @param description
	 */
	private Status(String description) {
		this.description = description;
	}


}
