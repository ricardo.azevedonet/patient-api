package br.com.hospital.patient.generic.util;

import org.springframework.context.ApplicationContext;


public class ServiceLocator {

	private static ServiceLocator instance;
	
	private ApplicationContext applicationContext;
	
	private ServiceLocator() {}	
	
	public static ServiceLocator getInstance() {
		return instance == null ? instance = new ServiceLocator() : instance;
	}
	
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;	
	}
	
	
	public Object getBean(Class<?> c) {
		return applicationContext.getBean(c);
	}
}