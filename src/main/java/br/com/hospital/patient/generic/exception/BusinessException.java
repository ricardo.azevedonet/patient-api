package br.com.hospital.patient.generic.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public abstract class BusinessException extends Exception {
	private static final long serialVersionUID = 5879267262758190898L;

	public abstract String getCdError();

}
