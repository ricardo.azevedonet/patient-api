package br.com.hospital.patient.generic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.hospital.patient.generic.dto.EnderecoDTO;
import br.com.hospital.patient.generic.service.EnderecoService;

@RestController
@RequestMapping("/endereco/v1")
public class EnderecoController {
	
	@Autowired
	private EnderecoService enderecoService;

	@GetMapping("/{cep}")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public ResponseEntity<EnderecoDTO> search(@PathVariable String cep) {
		return ResponseEntity.ok(enderecoService.obtemEnderecoPorCep(cep));
	}

}
