package br.com.hospital.patient.generic.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class MessagesDTO implements Serializable {

	private static final long serialVersionUID = 7561507178742163677L;

	private String chave;

	private String valor;
}
