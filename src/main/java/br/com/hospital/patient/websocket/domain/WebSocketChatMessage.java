package br.com.hospital.patient.websocket.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class WebSocketChatMessage {
	private String type;
	private String content;
	private String sender;
}
