package br.com.hospital.patient.websocket.controller;

import br.com.hospital.patient.websocket.domain.WebSocketChatMessage;
import br.com.hospital.patient.websocket.dto.NotificationDTO;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

@Controller
public class WebSocketChatController {

	@MessageMapping("/chat.sendMessage")
	@SendTo("/topic/javainuse")
	public WebSocketChatMessage sendMessage(@Payload WebSocketChatMessage webSocketChatMessage) {
		return webSocketChatMessage;
	}

	@MessageMapping("/chat.newUser")
	@SendTo("/topic/notification")
	public WebSocketChatMessage addUser(@Payload WebSocketChatMessage webSocketChatMessage,
			SimpMessageHeaderAccessor headerAccessor) {
		headerAccessor.getSessionAttributes().put("username", webSocketChatMessage.getSender());
		return webSocketChatMessage;
	}

	@MessageMapping("/chat.notification")
	@SendTo("/topic/notification")
	public NotificationDTO sendMessageNotification(@Payload WebSocketChatMessage webSocketChatMessage) {
		NotificationDTO n = new NotificationDTO();

		n.setMessage(webSocketChatMessage.getContent());
		n.setCreatedOn(LocalDateTime.now());
		n.setUserId(1l);

		return n;
	}


}
