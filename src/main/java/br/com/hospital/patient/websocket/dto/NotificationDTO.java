package br.com.hospital.patient.websocket.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class NotificationDTO {

    LocalDateTime createdOn;
    Long userId;
    String message;
    String jsonMessage;
}
