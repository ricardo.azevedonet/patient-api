package br.com.hospital.patient;

import javax.jms.Topic;

import org.apache.activemq.command.ActiveMQTopic;
import org.h2.server.web.WebServlet;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import br.com.hospital.patient.generic.util.ServiceLocator;



@SpringBootApplication
public class PatientApiApplication implements CommandLineRunner {

	@Autowired
	private ApplicationContext applicationContext;

	public static void main(String... args) {
		SpringApplication.run(PatientApiApplication.class);		
	}

	@Bean("ModelMapper")
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		return modelMapper;
	}

	@SuppressWarnings("rawtypes")
	@Bean
	ServletRegistrationBean h2servletRegistration() {
		@SuppressWarnings("unchecked")
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
		registrationBean.addUrlMappings("/console/*");
		return registrationBean;
	}

	@Bean(name = "/topic/javainuse")
	public Topic patientInsertSystem() {
		return new ActiveMQTopic("/topic/javainuse");
	}

	@Override
	public void run(String... args) throws Exception {
		ServiceLocator.getInstance().setApplicationContext(applicationContext);		
	}

}
