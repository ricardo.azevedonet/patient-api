delete from user_roles;
delete from users;
delete from roles;
delete from patient;

INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');

insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('ricardoxxx@gmail.com', 'Ricardo', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'ricardo.azevedo', null, 'A', CURRENT_TIMESTAMP() );
insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('admin@gmail.com', 'admin', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'admin', (select id from users where username = 'ricardo.azevedo') , 'A', CURRENT_TIMESTAMP() );
insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('joao@gmail.com', 'joao', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'joao', (select id from users where username = 'ricardo.azevedo') , 'A', CURRENT_TIMESTAMP() );
insert into users ( email, name, password, username, user_change_id, status, date_change ) values ('barbara@gmail.com', 'barbara', '$2a$10$waX/ispCltT3Wa6z4roAI.2ngP58Yu9tte0NuXWx4Gr/sH.fRASry', 'barbara', (select id from users where username = 'ricardo.azevedo'), 'A', CURRENT_TIMESTAMP() );

insert into user_roles (user_id, role_id) values ((select id from users where username = 'ricardo.azevedo'), (select id from roles where name = 'ROLE_USER' ));
insert into user_roles (user_id, role_id) values ((select id from users where username = 'ricardo.azevedo'), (select id from roles where name = 'ROLE_ADMIN' ));

insert into user_roles (user_id, role_id) values ((select id from users where username = 'barbara'), (select id from roles where name = 'ROLE_USER' ));
insert into user_roles (user_id, role_id) values ((select id from users where username = 'barbara'), (select id from roles where name = 'ROLE_ADMIN' ));


insert into user_roles (user_id, role_id) values ((select id from users where username = 'admin'), (select id from roles where name = 'ROLE_ADMIN' ));

insert into user_roles (user_id, role_id) values ((select id from users where username = 'joao'), (select id from roles where name = 'ROLE_USER' ));


insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '67723688008', 'maria@gmail.com', 'maria', (select id from users where username = 'ricardo.azevedo'), 'A', CURRENT_TIMESTAMP(),
    'Avenida Vinte e Três de Maio', 2499, '', 'Bela Vista',  'São Paulo', 'São Paulo', '01316060', '11965478543', PARSEDATETIME(FORMATDATETIME('2000-02-11', 'yyyy-MM-dd'), 'yyyy-MM-dd')
     );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '10082984093', 'conceicao@gmail.com', 'conceicao', (select id from users where username = 'ricardo.azevedo'), 'A', CURRENT_TIMESTAMP(),
    'Praça da Sé', 28, '', 'Sé',  'São Paulo', 'São Paulo', '01001001', '11912659874', PARSEDATETIME(FORMATDATETIME('1982-02-28', 'yyyy-MM-dd'), 'yyyy-MM-dd') 
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '92335704010', 'thais@gmail.com', 'thais', (select id from users where username = 'ricardo.azevedo'), 'A', CURRENT_TIMESTAMP(),
    'Rua Gilberto Sabino', 54, '', 'Pinheiros',  'São Paulo', 'São Paulo', '05425020', '11936141523', PARSEDATETIME(FORMATDATETIME('1990-01-26', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '40725725001', 'robson@gmail.com', 'robson', (select id from users where username = 'ricardo.azevedo'), 'A', CURRENT_TIMESTAMP(),
    'Avenida Paulista', 900, '', 'Bela Vista', 'São Paulo', 'São Paulo', '01310940', '11985142536', PARSEDATETIME(FORMATDATETIME('1999-12-10', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );

insert into patient (cpf, email, name, user_change_id, status, date_change,
    address, number, complement, district, city, state, zip_code, telefone, date_birth ) values (
    '40725725001', 'carlos@gmail.com', 'carlos', (select id from users where username = 'ricardo.azevedo'), 'A', CURRENT_TIMESTAMP(),
    'Praça Carlos Gomes', 46, '10º Andar', 'Centro', 'São Paulo', 'São Paulo', '01501040', '1122282071', PARSEDATETIME(FORMATDATETIME('1979-01-06', 'yyyy-MM-dd'), 'yyyy-MM-dd')
    );




