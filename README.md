# springboot-patient-api

[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Mínimo [Spring Boot](http://projects.spring.io/spring-boot/) patient-api

## Requisitos

Para construir e executar a aplicação de que necessita:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Execução da aplicação localmente

Há várias maneiras de executar uma aplicação Spring Boot na sua máquina local. Uma maneira é executar o método "principal" na `br.com.hospital.patient.PatientApiApplication` classe do seu IDE.

Em alternativa, pode utilizar o [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
E ainda  possível executar por linha de comando, usando
```shell
java -jar target/patient-api-0.0.1-SNAPSHOT.jar
```


## Para este projeto usamos as seguintes tecnologias/metodologias:

Tecnologias:

* Springboot
* Spring Rest
* Spring Secutiry com JWT
* Spring data
* Banco de dados H2
* Service called "springboot-sample-app"

Metodologias:

* Orientado a objetos {
    herança, encapsulamento, polimorfismo, entidades não anemicas
}
* DDD (Domain-Driven Design){
    linguagem ubíqua, separacção por domínio, dominio generico, servicos, entidades, repositorios
}

## Url projeto

host: http://localhost:8080/patient-api

## para acesso ao banco os usuários se senha são:

| Usuário | Senha | Perfil |
| :---: | :---: | :---: | 
| ricardo.azevedo | 123456 | ADMIN e USER |
| barbara | 123456 |  ADMIN e USER |
| admin | 123456 | ADMIN |
| joao | 123456 | USER |


## Copyright

Released under the Apache License 2.0. See the [LICENSE](https://github.com/codecentric/springboot-sample-app/blob/master/LICENSE) file.